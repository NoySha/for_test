<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Interview extends Model
{
    protected $fillable = ['description','date','candidate_id', 'user_id'];

    public function candidate(){
        return $this->belongsTo('App\Candidate');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function thereisinterview($uid){
        $thereis = DB::table('interviews')->where('user_id',$uid)->get();
        if(isset($thereis)) return true;
        return false;
    }


}

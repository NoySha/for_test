<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidates()
    {
        return $this->hasMany('App\Candidate');
    } 

    public static function next($status_id){
        // pluck שולף עמודה מסויימת מתוך הטבלה
        // יהיה מערך של איי די שאליהם ניתן לעבור מהאי דיי שקיבלנו
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        // הסטטוסים שאליהם אפשר לעבור
        return self::find($nextstages)->all(); 
    }
    // get לוקח את כל הרשומה 
    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
    }

}

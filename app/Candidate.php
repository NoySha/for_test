<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    // for mass asigment
    protected $fillable = ['name','email'];

    // אם השם של הפונקציה זהה לשם של השדה כמו בסטטוס, אין צורך לציין את השדה המחבר
    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     

    public function interviews(){
        return $this->hasMany('App\Interview');
    }

}

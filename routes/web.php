<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// אוסף של ראוטרים
//middleware('auth') - מי שלא רשום לאתר לא יכול להיכנס לראות את הדברים
Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');

Route::get('interviews', 'CandidatesController@interviews')->name('candidates.interviews')->middleware('auth');
Route::get('createinterview', 'CandidatesController@createinterview')->name('candidates.createinterview')->middleware('auth');
Route::get('storeinterview', 'CandidatesController@storeinterview')->name('candidates.storeinterview')->middleware('auth');
Route::get('myinterviews', 'CandidatesController@myinterviews')->name('candidates.myinterviews')->middleware('auth');



// דורסים את הראוט מסוג דיליט שנוצר באופן אוטומטי כי הוא לא מסוג get. 
// אנו לא רוצים להגיע אליו מטופס, אלא מקישור
Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('/hello', function (){
    return 'Hello Larevel';
}); 

Route::get('/student/{id}', function ($id  = 'No student found'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}', function ($id  = null){
    if(isset($id)){
        //TODO: validate for integer  
        return "We got car $id";
    } else {
        return 'We need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

Route::get('/users/{email}/{name?}', function ($email,$name = null) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});




#example
Route::get('/users1/{name?}/{email}', function ($name = null, $email) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

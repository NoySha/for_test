<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            // צריך להיות מוגדר כמו השדה איי די ביוזר
            //unsigned() לא יכול לקבל מינוס
            //nullable() יכול לקבל null
            // index עוזר להגיע מהר לערך המבוקש
            $table->bigInteger('user_id')->unsigned()->nullable()->index()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}

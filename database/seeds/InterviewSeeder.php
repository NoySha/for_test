<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'description' => 'not too good',
                'date' => '2020-01-02 17:12:10',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
             
            ],
            [
                'description' => 'very good',
                'date' => '2020-01-01 17:12:20',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);            
    }
}

@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create interview</h1>
        <form method = "get" action = "{{route('candidates.storeinterview')}}">      
        @csrf 
        <div class="form-group">
            <label for = "description">description</label>
            <input type = "text" class="form-control" name = "description">
        </div>     
        <div class="form-group">
            <label for = "date">date</label>
            <input type = "text" class="form-control" name = "date">
        </div>                      
        

        <label for = "user">user</label>
                <select class="form-control" name="user_id">                                                                         
                        @foreach ($users as $user)
                                     <option value="{{ $user->id }}"> 
                                         {{ $user->name }} 
                                     </option>
                        @endforeach  
                </div>  
                </select>

                
        <label for = "candidate">candidate</label>
                <select class="form-control" name="candidate_id">                                                                         
                        @foreach ($candidates as $candidate)
                                     <option value="{{ $candidate->id }}"> 
                                         {{ $candidate->name }} 
                                     </option>
                        @endforeach 
                </div>     
                </select>

        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>  

        </form>    
@endsection

@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create candidate</h1>
        <!-- action send the details from the form to the function in controler (she knows the route) -->
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        <!-- מרכיב של אבטחת מידע -->
        @csrf 
        <div class="form-group">
            <label for = "name">Candiadte name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "email">Candiadte email</label>
            <input type = "text" class="form-control" name = "email">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create candidate">
        </div>                       
        </form>    
@endsection

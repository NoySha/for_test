@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>description</th><th>date</th> <th>candidate</th> <th>user</th> 
    </tr>
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->description}}</td>                             
            <td>{{$interview->date}}</td>
            <td>{{$interview->candidate->name}}</td>
            <td>{{$interview->user->name}}</td>



    @endforeach
</table>

<a class="navbar-brand" href="{{ route('candidates.createinterview') }}">
    Create interview
</a> 
@endsection


@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if (App\Interview::thereisinterview($user) == FALSE )

<h1>My interviews</h1>

<table class = "table table-dark">
    <tr>
        <th>description</th><th>date</th> 
    </tr>
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->description}}</td>                             
            <td>{{$interview->date}}</td>

    @endforeach
</table>

@else
<h4> you have not interviews!!!</h4>
@endif

@endsection

